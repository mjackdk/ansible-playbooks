---
- name: Remove default packages
  ansible.builtin.package:
    name:
      - gnome-tour
      - rhythmbox
    state: absent
  become: true

- name: Add extra packages
  ansible.builtin.package:
    name:
      - ansible
      - ansible-lint
      - arm-image-installer
      - butane
#      - cockpit
      - coreos-installer
      - distrobox
      - fastfetch
      - geary
      - gnome-music
      - gstreamer1-plugin-openh264
      - minicom
      - mozilla-openh264
      - nextcloud-client-nautilus
      - powerline-go
      - python3-psutil
      - rpi-imager
      - vim
      - yamllint
    state: present
  become: true

- name: Add Pico SDK support
  ansible.builtin.package:
    name:
      - arm-none-eabi-binutils
      - arm-none-eabi-gcc-cs
      - arm-none-eabi-gcc-cs-c++
      - arm-none-eabi-newlib
      - cmake
      - libftdi-devel
      - libusb1
      - libusb1-devel
      - '@C Development Tools and Libraries'
    state: present
  become: true

- name: Enable vscode repository
  ansible.builtin.yum_repository:
    name: "vscode"
    description: "Visual Studio Code"
    baseurl: "https://packages.microsoft.com/yumrepos/vscode"
    gpgkey: "https://packages.microsoft.com/keys/microsoft.asc"
    gpgcheck: true
    state: present
  become: true

- name: Install vscode
  ansible.builtin.package:
    name: code
    state: present
  become: true

- name: Enable vscodium repository
  ansible.builtin.yum_repository:
    name: "vscodium"
    description: "VSCodium"
    baseurl: "https://download.vscodium.com/rpms/"
    gpgkey: "https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg"
    gpgcheck: true
    state: present
  become: true

- name: Install vscodium
  ansible.builtin.package:
    name: codium
    state: present
  become: true

- name: Add flathub flatpak repository remote as user installation
  community.general.flatpak_remote:
    name: flathub
    state: present
    flatpakrepo_url: https://dl.flathub.org/repo/flathub.flatpakrepo
    method: user

- name: Install packages from flathub for current user
  community.general.flatpak:
    name:
      - cc.arduino.IDE2
      - com.bitwarden.desktop
      - com.github.johnfactotum.Foliate
      - com.mikrotik.Winbox
      - io.podman_desktop.PodmanDesktop
      - org.gnome.World.Secrets
    state: present
    method: user

- name: Add user to additional groups
  ansible.builtin.user:
    name: "{{ lookup('env', 'USER') }}"
    groups:
      - dialout
    append: true
  become: true

#- name: Enable Cockpit socket
#  ansible.builtin.systemd_service:
#    name: cockpit.socket
#    state: started
#    enabled: true

- name: Copy background in, if not found
  ansible.builtin.copy:
    src: files/background.jpg
    dest: ~/.background.jpg
    mode: '0644'

- name: Set background
  community.general.dconf:
    key: "/org/gnome/desktop/background/picture-uri"
    value: "'file:///home/{{ ansible_user_id }}/.background.jpg'"

- name: Show Calendar week number
  community.general.dconf:
    key: "/org/gnome/desktop/calendar/show-weekdate"
    value: "true"

- name: Show Clock date
  community.general.dconf:
    key: "/org/gnome/desktop/interface/clock-show-date"
    value: "false"

- name: Show Clock weekday
  community.general.dconf:
    key: "/org/gnome/desktop/interface/clock-show-weekday"
    value: "true"

- name: Terminal font
  community.general.dconf:
    key: "/org/gnome/Ptyxis/font-name"
    value: "'Monospace 12'"

- name: Terminal use system font
  community.general.dconf:
    key: "/org/gnome/Ptyxis/use-system-font"
    value: "false"

- name: TextEditor custom font
  community.general.dconf:
    key: "/org/gnome/TextEditor/custom-font"
    value: "'Monospace 12'"

- name: TextEditor use system font
  community.general.dconf:
    key: "/org/gnome/TextEditor/use-system-font"
    value: "false"

- name: Create bashrc.d directory, if not found
  ansible.builtin.file:
    path: ~/.bashrc.d
    state: directory
    mode: '0755'

- name: Copy bashrc.local in, if not found
  ansible.builtin.copy:
    src: files/bashrc.local
    dest: ~/.bashrc.d/bashrc.local
    mode: '0644'

- name: Copy vimrc in, if not found
  ansible.builtin.copy:
    src: files/vimrc
    dest: ~/.vimrc
    mode: '0644'
