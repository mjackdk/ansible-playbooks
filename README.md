# Ansible Playbooks

## Desktop

Tested on Debian Bookworm and Fedora Workstation 39 with default settings.

### Debian

```
$ sudo apt update
$ sudo apt install ansible
```

### Fedora Workstation
```
$ sudo dnf update
$ sudo dnf install ansible
```

### Run playbook

```
$ ansible-playbook --ask-become-pass --limit localhost main.yml
```

### After
* Firefox sync
* Nextcloud sync
* SSH keys
* GPG keys
* [powerline-go](https://github.com/justjanne/powerline-go)
* [pyenv](https://github.com/pyenv/pyenv)

